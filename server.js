const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")

//View all closed/open tickets
app.use("/",require("./controller/ticketStatus"));

//adding buses with details of routes and timing
app.use("/",require("./controller/addingBus"));

//Booking a ticket by providing user details
app.use("/",require("./controller/Booking"));

//Booking a ticket with seat number by providing user details
app.use("/",require("./controller/bookingBySeatNo"));

//View Details of person owning the ticket.
app.use("/",require("./controller/fetchingTicketDetails"));

//Update the ticket status (open/close + adding user details)
app.use("/",require("./controller/cancellation"));

//API for admin to reset the server (opens up all the tickets)
app.use("/",require("./controller/RestartingTheServer"));


//database connection
require("./mongo")

//Models
require("./model/Post")

const url = require('url');





app.use(bodyParser.json())

const PORT = 8082;
const HOST = '0.0.0.0';
app.listen(PORT, HOST);
console.log(`Running on http://${HOST}:${PORT}`);

module.exports=app;