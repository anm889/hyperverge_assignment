const mongoose = require("mongoose");

const Bus_schema = mongoose.Schema({
    Bus_Number:
    {
        type: String,
        required: true
    },
    Total_Seats:
    {
        type: Number,
        required: true
    },
    Seats_Array:
    {
        type: Array,
        required: true
    },
    From:
    {
        type: String,
        required: true
    },
    To:
    {
        type: String,
        required: true
    },
    Start_Time:
    {
        type: String,
        required: true
    },
    End_Time:
    {
        type: String,
        required: true
    },
    DateofJourney:
    {
        type: String,
        required: true
    }
    
})

module.exports=mongoose.model("Buses",Bus_schema)