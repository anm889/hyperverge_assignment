const mongoose = require("mongoose");


const Ticket_schema = mongoose.Schema({
    Bus_Id:
    {
        type: String,
        required: true 
    },
    Ticket_No:
    {
        type: String,
        required: true 
    },
    Seat_Number:
    {
        type: Number,
        required: true
    },
    BookingDate:
    {
        type: String,
        required: true
    },
    Bus_Number:
    {
        type: String,
        required: true
    },
    Status:
    {
        type: String,
        required: true
    },    
})

module.exports=mongoose.model("Tickets",Ticket_schema)