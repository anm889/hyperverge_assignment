const mongoose = require("mongoose");

const User_schema = mongoose.Schema({
    Ticket_No:
    {
        type: String,
        required: true 
    },
    Name:
    {
        firstName:
        {
            type: String,
            required: true      
        },
        lastName:
        {
            type: String,
            required: true     
        }
    },
    Age:
    {
        type: Number,
        required: true
    },
    Contact_Number:
    {
        type: String,
        required: true
    }
})
module.exports=mongoose.model("Users",User_schema)