# Ticket-Booking-System

This project is a part of Hyperverge-Assignment

# Problem Statememt

To Setup a NodeJS server on EC2 to handle ticketing for a bus company.

# Features from the server:

* Update the ticket status (open/close + adding user details)
* View Ticket Status
* View all closed tickets
* View all open tickets
* View Details of person owning the ticket.
* Additional API for admin to reset the server (opens up all the tickets)

Use an appropriate database to handle the problem. The server needs to expose the features via APIs based on REST principles and event driven logic to be implemented in every possible situation.
Additionally, write appropriate test cases to simulate practical scenarios that you would want to test the system for. Maintain the code through the course of the development on a version control system.

# Installation

1. Install node from nvm.
2. Install mongodb.
3. Clone the repository.
4. Run npm install inside the directory
5. Make .env file and put it in base directory.
6. Run npm test to test npm server.js to deploy.

# Documentation

**Hit Api's in order as given below**
1. Add New Bus to db by providing following details(**Url**- http://13.234.20.180:80/adding_Bus)
   * Bus_Number
   * Total_Seats
   * From
   * To
   * Start_Time
   * End_Time
   * DateofJourney <br/>
  

2. Book Tickets by providing following details(**Url**- http://13.234.20.180:80/bookTicket/A22 )
   * Name
     * FirstName
     * LastName
   * Age
   * Contact Number
   
 
  
3. Book a ticket for a specific Seat by providing following details (**Url**- http://13.234.20.180:80/bookTicket/A22/19 )
   * Seat Number
   * Name
     * FirstName
     * LastName
   * Age
   * Contact Number


4. User can fetch the details of any person by providing Ticket Number(**Url**- http://13.234.20.180:80/fetchTicketDetails/1586777472333)


5. User can view open/close Tickets for seat by providing Bus Number(**Url**- http://13.234.20.180:80/view_tickets_status/A22?status=open)


6. Admin can reset the server by Providing Bus Number (**Url**- http://13.234.20.180:80/restarting-server/A22)
  

7. User can cancel the ticket by Providing Ticket Number  (**Url**-  http://13.234.20.180:80/ticketCancellation/158671123664)
 








