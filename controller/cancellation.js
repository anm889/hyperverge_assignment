const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');

require("../model/ticketSchema");
require("../model/busSchema");
require("../model/userSchema");

const Post= mongoose.model("Tickets")

const Buses=mongoose.model("Buses")

const Users=mongoose.model("Users")

router.get("/ticketCancellation/:TicketId", async (req,res)=>
{
    try 
    {
        const ticket = await Post.findOne({Ticket_No:req.params.TicketId});
        //console.log(ticket);
        if(ticket==null)
        {
            res.send("Please Enter Valid Ticket_Number")
        }
        else if(ticket.Status=="closed")
        {
            res.send("Ticket is already closed/cancelled")
        }
        else
        {
            const ticket1 =  await Post.findByIdAndUpdate({ _id: ticket["_id"]},{Status:"closed"},{new:true})
            const bus = await Buses.findOne({_id:ticket.Bus_Id});
            console.log(bus.Seats_Array[ticket.Seat_Number-1]);
            bus.Seats_Array[ticket.Seat_Number-1]=0;
            
            var arr = new Array(bus.Seats_Array.length);
            
            arr=bus.Seats_Array
            
            const bus1 =  await Buses.findByIdAndUpdate({ _id: bus["_id"]},{Seats_Array:arr},{new:true})
            res.send("Ticket Cancellation is Successfull");
        }
        
    } 
    catch (error) 
    {
        res.status(500)
    }
})

module.exports=router

