const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');

require("../model/busSchema")

const Buses=mongoose.model("Buses")


router.post('/adding_Bus',(req,res)=>{
  try 
  {
    const buses = new Buses();

    buses.Bus_Number=req.body.Bus_Number;
    buses.Total_Seats=req.body.Total_Seats;
    buses.From=req.body.From;
    buses.To=req.body.To;
    buses.Start_Time=req.body.Start_Time;
    buses.End_Time=req.body.End_Time;
    buses.DateofJourney=req.body.DateofJourney;
    
    var arr = new Array(req.body.Total_Seats)
    for(var i=0;i<req.body.Total_Seats;i++)
    {
        arr[i]=0;
    }
    buses.Seats_Array=arr;
     buses.save();
    res.send(buses)
    
      
  } catch (error) 
  {
      res.status(500)
  }


})
module.exports=router

