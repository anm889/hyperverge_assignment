const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');

require("../model/busSchema")
require("../model/ticketSchema")

const Buses=mongoose.model("Buses")
const Post= mongoose.model("Tickets")


router.get('/restarting-server/:Bus_Number',async (req,res)=>{
  try 
  {
    
    const bus = await Buses.findOne({Bus_Number:req.params.Bus_Number})
    
    for(var i=0;i<bus.Seats_Array.length;i++)
    {
        bus.Seats_Array[i]=0;
    }
    var arr = new Array(bus.Seats_Array.length) 
    arr=bus.Seats_Array
    await Buses.findByIdAndUpdate({ _id:bus["_id"]},{Seats_Array:arr},{new:true})
    await Post.updateMany({ Bus_Number:req.params.Bus_Number},{Status:"closed"},{new:true})

    res.send("Server Resetting is successfull")
    
      
  } catch (error) 
  {
      res.status(500)
  }


})
module.exports=router

