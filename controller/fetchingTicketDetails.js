const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');

require("../model/ticketSchema");
require("../model/busSchema");
require("../model/userSchema");

const Post= mongoose.model("Tickets")

const Buses=mongoose.model("Buses")

const Users=mongoose.model("Users")

router.get("/fetchTicketDetails/:TicketId", async (req,res)=>{
    try 
    {
        const user = await Users.findOne({Ticket_No:req.params.TicketId})

        res.send(user)
    } 
    catch (error) 
    {
        res.status(500)
    }
})

module.exports=router

