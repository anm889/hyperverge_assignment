const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');


require("../model/ticketSchema")
require("../model/busSchema")
require("../model/userSchema")

const Post= mongoose.model("Tickets")

const Buses=mongoose.model("Buses")

const Users=mongoose.model("Users")


router.post("/bookTicket/:Bus_Number/:Seat_Number", bodyParser.json(),async (req,res)=>{
    try 
    {   
        const bus = await Buses.findOne({Bus_Number:req.params.Bus_Number})
        if(bus.Seats_Array[req.params.Seat_Number-1]==0)
        {
            bus.Seats_Array[req.params.Seat_Number-1]=1
            var arr = new Array(bus.Seats_Array.length) 
            arr=bus.Seats_Array
            const bus1 =  await Buses.findByIdAndUpdate({ _id: bus["_id"]},{Seats_Array:arr},{new:true})

            var d = new Date(),
            seconds = d.getSeconds().toString().length == 1 ? '0'+d.getSeconds() : d.getSeconds(),
            minutes = d.getMinutes().toString().length == 1 ? '0'+d.getMinutes() : d.getMinutes(),
            hours = d.getHours().toString().length == 1 ? '0'+d.getHours() : d.getHours(),
            ampm = d.getHours() >= 12 ? 'pm' : 'am',
            months = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'],
            days = ['Sunday','Monday','Tuesday','Wednesday','Thursday','Friday','Saturday'];
            var date=days[d.getDay()]+' '+d.getDate()+' '+months[d.getMonth()]+' '+d.getFullYear()+' '+hours+':'+minutes+':'+seconds;

            var timestamp = new Date().getTime();
    
            const post = new Post();
            const user = new Users();
            post.Ticket_No=timestamp;
            post.Seat_Number=req.params.Seat_Number;
            post.Bus_Number=req.params.Bus_Number;
            post.Status="open";
            post.BookingDate=date;
            post.Bus_Id=bus["_id"]
            user.Name.firstName=req.body.Name.firstName;
            user.Name.lastName=req.body.Name.lastName;
            user.Age=req.body.Age;
            user.Contact_Number=req.body.Contact_Number;
            user.Ticket_No=timestamp;
            await post.save();
            await user.save();
            res.send(user)
        }
        else
        {
            res.send("Entered Seat is already booked, Please choose any other Seat")
        }
    } catch (error) 
    {
        res.status(500)
    }
})

module.exports=router