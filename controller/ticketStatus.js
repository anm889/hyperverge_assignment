const express=require("express");
const app=express();
const mongoose=require("mongoose")
const bodyParser=require("body-parser")
const router=express.Router()


router.use(bodyParser.json())

const url = require('url');

require("../model/busSchema")

const Buses=mongoose.model("Buses")


router.get("/view_tickets_status/:Bus_Number",async (req,res)=>{

    try 
    {
        
        const bus = await Buses.findOne({Bus_Number:req.params.Bus_Number})
       
        const queryObject = url.parse(req.url,true).query;
        //console.log(queryObject['status']);
        
        var responseData={}
        var arr=[];
        if(queryObject['status']=='open')
        {
            for(var i=0;i<bus.Seats_Array.length;i++)
            {
                if(bus.Seats_Array[i]==0)
                {
                     arr.push(i+1);
                }
            }
            responseData['open_tickets']=arr;
            res.send(responseData);
        }
        else if(queryObject['status']=='close')
        {
            for(var i=0;i<bus.Seats_Array.length;i++)
            {
                if(bus.Seats_Array[i]==1)
                {
                     arr.push(i+1);
                }
            }
            responseData['closed_tickets']=arr;
            res.send(responseData);
        }
        else
        {
            res.send("Wrong status, select open/close")

        }
        
    } catch (error) 
    {

        res.status(500)
    }
})

module.exports=router