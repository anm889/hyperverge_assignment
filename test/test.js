var fs = require('fs');
var content = fs.readFileSync('test/sample.json','utf8');
const jsonobj = JSON.parse(content);

var app = require('../server.js'),
assert = require('assert'),
chai = require('chai'),
should = require('should');
request = require('supertest');



//testing AddingBus api
describe('1. AddingBus api testing', function() {
  it('post request', function(done) {
    request(app)
    .post('/adding_Bus')
    .send({Bus_Number:jsonobj['AddingBus']["Input"]["Bus_Number"], Total_Seats: jsonobj['AddingBus']["Input"]["Total_Seats"]})
    .set('Accept', 'application/json')
    .expect('Content-Type', /json/)
    .expect(200).end(function(err,res){
      res.status.should.equal(200);
      res.body.Bus_Number.should.equal(jsonobj['AddingBus']["Output"]["Bus_Number"]);
      res.body.Total_Seats.should.equal(jsonobj['AddingBus']["Output"]["Total_Seats"]);
      done();
    });
  });
});

//testing Ticket Booking api
describe('2. Ticket Booking api testing', function() {
    it('post request', function(done) {
      request(app)
      .post('/bookTicket/'+jsonobj["Booking"]["Input"]["Bus_Number"])
      .send({
        Name:
        {
            firstName:jsonobj["Booking"]["Input"]["Name"]["firstName"],
            lastName:jsonobj["Booking"]["Input"]["Name"]["lastName"]
        },
        Age:jsonobj["Booking"]["Input"]["Age"],
        Contact_Number:jsonobj["Booking"]["Input"]["Contact_Number"]
})
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200).end(function(err,res){
        res.status.should.equal(200);
        done();
      });
    });
  });

//testing Ticket-Booking-By-Seat-Number
  describe('3. Ticket-Booking-By-Seat-Number api testing', function() {
    it('post request', function(done) {
      request(app)
      .post('/bookTicket/'+jsonobj["BookingBySeatNumber"]["Input"]["Bus_Number"]+'/'+jsonobj["BookingBySeatNumber"]["Input"]["Seat_Number"])
      .send({
        Name:
        {
            firstName:jsonobj["BookingBySeatNumber"]["Input"]["Name"]["firstName"],
            lastName:jsonobj["BookingBySeatNumber"]["Input"]["Name"]["lastName"]
        },
        Age:jsonobj["BookingBySeatNumber"]["Input"]["Age"],
        Contact_Number:jsonobj["Booking"]["Input"]["Contact_Number"]
})
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200).end(function(err,res){
        res.status.should.equal(200);
        done();
      });
    });
  });

  //testing fetchingTicketDetails api
  describe('4. fetchingTicketDetails api testing', function() {
    it('get request', function(done) {
      request(app)
      .get('/fetchTicketDetails/'+jsonobj['fetchingTicketDetails']["Input"]["Ticket_No"])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200).end(function(err,res){
        res.status.should.equal(200);
        res.body.Name.firstName.should.equal(jsonobj['fetchingTicketDetails']["Output"]["Name"]["firstName"]);
        res.body.Name.lastName.should.equal(jsonobj['fetchingTicketDetails']["Output"]["Name"]["lastName"]);
        res.body.Age.should.equal(jsonobj['fetchingTicketDetails']["Output"]["Age"]);
        res.body._id.should.equal(jsonobj['fetchingTicketDetails']["Output"]["_id"]);
        res.body.Contact_Number.should.equal(jsonobj['fetchingTicketDetails']["Output"]["Contact_Number"]);
        res.body.Ticket_No.should.equal(jsonobj['fetchingTicketDetails']["Output"]["Ticket_No"]);
        done();
      });
    });
  });

  // testing open-ticket-Status api
  describe('5. view-all-open/close-tickets api testing', function() {
    it('get request', function(done) {
      request(app)
      .get('/view_tickets_status/'+jsonobj['view-all-open/close-tickets']["Input"]["Bus_Number"]+'?'+jsonobj['view-all-open/close-tickets']["Input"]["Status"])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200).end(function(err,res){
      res.status.should.equal(200);
      done();
      });
    });
  });
  
  //updating-ticket-Status api testing
  describe('6. updating-ticket-Status api testing', function() {
    it('get request', function(done) {
      request(app)
      .get('/ticketCancellation/'+jsonobj['updating-ticket-Status']["Input"]["Ticket_No"])
      .set('Accept', 'application/json')
      .expect('Content-Type', /json/)
      .expect(200).end(function(err,res){
      res.status.should.equal(200);
      done();
      });
    });
  });